import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { Task } from '../models/task';
import { AngularFirestore } from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root'
})
export class TaskService {
  toDoChanged = new Subject<Task[]>();
  inProgressChanged = new Subject<Task[]>();
  qaChanged = new Subject<Task[]>();
  doneChanged = new Subject<Task[]>();
  constructor(
    private firestore: AngularFirestore
  ) {

  }

  initTasksObserver() {
    this.firestore.collection('todo').snapshotChanges().subscribe(snapshots => {
      const todos = snapshots.map(a => {
        const data = a.payload.doc.data() as Task;
        const Id = a.payload.doc.id;
        return { Id, ...data } as Task;
      });
      this.toDoChanged.next(todos);
    });

    this.firestore.collection('inProgress').snapshotChanges().subscribe(snapshots => {
      const inProgress = snapshots.map(a => {
        const data = a.payload.doc.data() as Task;
        const Id = a.payload.doc.id;
        return { Id, ...data } as Task;
      });
      this.inProgressChanged.next(inProgress);
    });

    this.firestore.collection('qa').snapshotChanges().subscribe(snapshots => {
      const qa = snapshots.map(a => {
        const data = a.payload.doc.data() as Task;
        const Id = a.payload.doc.id;
        return { Id, ...data } as Task;
      });
      this.qaChanged.next(qa);
    });

    this.firestore.collection('done').snapshotChanges().subscribe(snapshots => {
      const done = snapshots.map(a => {
        const data = a.payload.doc.data() as Task;
        const Id = a.payload.doc.id;
        return { Id, ...data } as Task;
      });
      this.doneChanged.next(done);
    });
  }

  add(boardName, task, index) {
    this.firestore.collection(boardName).add({
      Index: index,
      Name: task
    } as Task);
  }

  async update(boardName, itemId, item) {
    await this.firestore.collection(boardName).doc(itemId).set(item);
  }

  async delete(boardName, itemId) {
    await this.firestore.collection(boardName).doc(itemId).ref.delete();
  }
}
