import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { Router } from '@angular/router';
import { Auth } from '../models/auth';

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  isLoggedIn: any;
  constructor(
    private auth: AngularFireAuth,
    private router: Router
  ) {
    auth.authState.subscribe(state => {
      if (!state) {
        router.navigateByUrl("/login");
        this.isLoggedIn = false;
      } else {
        router.navigateByUrl("/home");
        this.isLoggedIn = true;
      }
    })
  }

  doLogin(email, password) {
    return new Promise((resolve, reject) => {
      this.auth.signInWithEmailAndPassword(email, password).then(response => {
        this.isLoggedIn = true;
        resolve(response.user);
      }).catch(error => {
        reject(error);
      })
    });
  }

  doLogOut() {
    this.auth.signOut();
  }

  isLoggedIng(): boolean {
    return this.isLoggedIn;
  }

  signUp(auth: Auth) {
    return new Promise((resolve, reject) => {
      this.auth.createUserWithEmailAndPassword(auth.email, auth.senha).then(response => {
        resolve(response.user);
      }).catch(error => {
        reject(error);
      });
    })
  }
}
