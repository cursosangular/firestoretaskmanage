import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatCardModule} from '@angular/material/card';
import {MatButtonModule} from '@angular/material/button';
import {MatBadgeModule} from '@angular/material/badge';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { NewTaskModalComponent } from './pages/home/new-task-modal/new-task-modal.component';
import { MatDialog, MatDialogModule } from '@angular/material/dialog';
import { FormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { AngularFireModule } from '@angular/fire';
import { environment } from '../environments/environment';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { IonicModule } from '@ionic/angular';
import { LoginService } from './core/services/login.service';
import { TaskService } from './core/services/task.service';
import { HomeComponent } from './pages/home/home.component';
import { LoginComponent } from './pages/login/login.component';
import { SignupComponent } from './pages/signup/signup.component';

@NgModule({
  declarations: [
    AppComponent,
    NewTaskModalComponent,
    HomeComponent,
    LoginComponent,
    SignupComponent
  ],
  entryComponents: [
    NewTaskModalComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatCardModule,
    MatButtonModule,
    DragDropModule,
    MatDialogModule,
    FormsModule,
    AngularFireModule.initializeApp(environment.firebase),
    MatFormFieldModule,
    MatInputModule,
    NgbModule,
    IonicModule.forRoot(),
    MatBadgeModule
  ],
  providers: [
    MatDialog,
    LoginService,
    TaskService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
