import { Component } from '@angular/core';
import { LoginService } from './core/services/login.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  isNavbarCollapsed = true;

  constructor(
    private loginService: LoginService
  ) {

  }

  logout() {
    this.loginService.doLogOut();
  }
}
