import { Component, OnInit } from '@angular/core';
import {CdkDragDrop, moveItemInArray, transferArrayItem} from '@angular/cdk/drag-drop';
import { MatDialog } from '@angular/material/dialog';
import { NewTaskModalComponent } from './new-task-modal/new-task-modal.component';
import { Task } from '../../core/models/task';
import { TaskService } from '../../core/services/task.service';
import { LoginService } from '../../core/services/login.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  title = 'angularTaskManager';

  todos: Task[];
  qa: Task[];
  inProgress: Task[];
  done: Task[];

  constructor(
    public dialog: MatDialog,
    private taskService: TaskService,
    private loginService: LoginService
  ) {

  }

  ngOnInit(): void {
    this.taskService.initTasksObserver();
    this.taskService.toDoChanged.subscribe(tasks => {
      this.todos = tasks;
    });

    this.taskService.qaChanged.subscribe(tasks => {
      this.qa = tasks;
    });

    this.taskService.inProgressChanged.subscribe(tasks => {
      this.inProgress = tasks;
    });

    this.taskService.doneChanged.subscribe(tasks => {
      this.done = tasks;
    });
  }

  compare(a, b) {
    let comparison = 0;
    if (a.Index > b.Index) {
      comparison = 1;
    } else if (a.Index < b.Index) {
      comparison = -1;
    }
    return comparison;
  }

  openDialog(listName): void {
    const dialogRef = this.dialog.open(NewTaskModalComponent, {
      width: '250px'
    });

    dialogRef.afterClosed().subscribe(result => {
      let index = 0;
      switch (listName) {
        case 'todo':
          index = this.todos.length
          break;
        case 'qa':
          index = this.qa.length
          break;
        case 'inProgress':
          index = this.inProgress.length
          break;
        case 'done':
          index = this.done.length
          break;
      }
      this.taskService.add(listName, result.Name, index);
    });
  }

  async deleteTask(listName, id) {
    if (confirm("Quer realmente deletar este item?")) {
      await this.taskService.delete(listName, id);
    };
  }

  async drop(event: CdkDragDrop<string[]>) {
    const item = (event.previousContainer.data[event.previousIndex] as any) as Task;
    item.Index = event.currentIndex;
    await this.taskService.delete(event.previousContainer.id, item.Id);
    await this.taskService.update(event.container.id, item.Id, item);
  }

}
