import { Component, OnInit } from '@angular/core';
import { Auth } from 'src/app/core/models/auth';
import { LoginService } from 'src/app/core/services/login.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {
  loginModel: Auth = {} as Auth;

  constructor(
    private loginService: LoginService,
    private router: Router
  ) { }

  ngOnInit(): void {
  }

  signUp() {
    this.loginService.signUp(this.loginModel).then(Response => {
      this.router.navigateByUrl("/home");
    }).catch(error => {
      alert("Error creating the user");
    });
  }
}
