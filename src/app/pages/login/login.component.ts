import { Component, OnInit } from '@angular/core';
import { Auth } from '../../core/models/auth';
import { LoginService } from 'src/app/core/services/login.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  loginModel: Auth = {} as Auth;
  showPassword: boolean;
  constructor(
    private loginService: LoginService,
    private router: Router
  ) { }

  ngOnInit(): void {
  }

  login() {
    this.loginService.doLogin(this.loginModel.email, this.loginModel.senha).then(response => {
      this.router.navigateByUrl("/home");
    }).catch(error => {
      alert("login ou senha inválidos");
    })
  }

}
