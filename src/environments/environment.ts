// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyB0EgVddBnj5kr5_2VK3S6RzuKoZ4mdsNA",
    authDomain: "taskmanager-18014.firebaseapp.com",
    databaseURL: "https://taskmanager-18014.firebaseio.com",
    projectId: "taskmanager-18014",
    storageBucket: "taskmanager-18014.appspot.com",
    messagingSenderId: "258593336920",
    appId: "1:258593336920:web:dccaf8b7bbec6faa52a42d"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
